package apps.menwithandroid.multiwebs;

import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.URLUtil;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static android.view.View.*;

public class MainActivity extends AppCompatActivity implements OnClickListener, Runnable {

    /* Final params */
    private static final String BASIC_WEB_ADDRESS = "http://adfoc.us/4164391";
    private static final int    TIME_TO_REFRESH   = 30000; //40 sec
    private static final String LOG_TAG           = "Roi";

    private static final String WEB_URL  = "http://free-proxy-list.net/";
    private static final String TABLE_ID = "proxylisttable";

    private static final int PROXY_HOST_INDEX = 1;
    private static final int PROXY_PORT_INDEX = 2;
    private static final int PROXY_TYPE_INDEX = 5;

    /* Views */
    private EditText     mWebAddressEditText = null;
    private EditText     mWebsNumberEditText = null;
    private LinearLayout mWebsLinearLayout   = null;
    private Button       mStartStopButton    = null;
    private Button       mISeeErrorButton    = null;

    /* Params */
    private String  mWebAddress  = BASIC_WEB_ADDRESS;
    private int     mWebsNumber  = 5;
    private boolean mRuiningFlag = false;

    private Thread  mRuiningThread    = new Thread(this);
    private boolean mThreadFinishLoad = false;

    private Proxy mCurrentProxy = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final boolean[] end = { false };
        new Thread(new Runnable() {
            @Override
            public void run() {
                Proxy.start();
                end[0] = true;
            }
        }).start();

        while(!end[0]);

        //Find views
        this.mWebAddressEditText = (EditText)     findViewById(R.id.WebAddressEditText);
        this.mWebsNumberEditText = (EditText)     findViewById(R.id.WebsNumberEditText);
        this.mWebsLinearLayout   = (LinearLayout) findViewById(R.id.WebsLinearLayout);
        this.mStartStopButton    = (Button)       findViewById(R.id.StartStopButton);
        this.mISeeErrorButton    = (Button)       findViewById(R.id.ISeeErrorButton);

        //Install and set params
        this.mWebAddressEditText.setText(this.mWebAddress);
        this.mWebsNumberEditText.setText(Integer.toString(this.mWebsNumber));

        this.mISeeErrorButton.setEnabled(false);

        this.mISeeErrorButton.setOnClickListener(this);
        this.mStartStopButton.setOnClickListener(this);
    }

    /**
     * Build the process
     */
    private void build() {
        this.mThreadFinishLoad = false;

        this.mCurrentProxy = null;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //Clear the old webs
                mWebsLinearLayout.removeAllViews();

                mCurrentProxy = Proxy.getRandom();

                for(int i = 0; i < mWebsNumber; i++) {
                    if(mCurrentProxy == null) {
                        return;
                    }

                    //Install web view
                    WebView thisWebView = new WebView(MainActivity.this);

                    //Set params
                    //Get proxy
                    ProxySettings.setProxy(
                            thisWebView,
                            mCurrentProxy.mHost,
                            mCurrentProxy.mPort,
                            getApplicationContext().getClass().getName()
                    );

                    final int finalI = i;
                    thisWebView.clearCache(true);
                    thisWebView.clearSslPreferences();
                    thisWebView.clearHistory();
                    thisWebView.clearFormData();
                    thisWebView.clearMatches();
                    thisWebView.getSettings().setJavaScriptEnabled(true);
                    thisWebView.setWebViewClient(new WebViewClient() {
                        @Override
                        public void onPageFinished(WebView view, String url) {
                            super.onPageFinished(view, url);

                            Log.d("Roi1", url);

                            if(finalI == mWebsNumber - 1) {
                                mThreadFinishLoad = true;
                            }
                        }

                        @Override
                        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                            super.onReceivedError(view, request, error);

                            mThreadFinishLoad = true;

                            if(mWebsNumber == 1) {
                                iSeeError(false);
                            }
                        }
                    });
                    thisWebView.loadUrl(mWebAddress);

                    mWebsLinearLayout.addView(thisWebView);
                }
            }
        });
    }

    /**
     * Function to run or stop the process
     */
    private void changeRunning() {
        //Change the mode
        if(this.mRuiningFlag) {
            //Stop
            stop();

        } else {
            //Start
            start();
        }
    }

    /**
     * Function to start the process
     */
    private void start() {
        //Get params
        if(!getParams()) {
            return;
        }

        this.mRuiningFlag = true;

        //Run
        this.mRuiningThread.interrupt();
        this.mRuiningThread = new Thread(this);
        this.mRuiningThread.start();

        //Change text to stop and enable and running flag
        this.mStartStopButton.setText(R.string.stop);

        this.mISeeErrorButton.setEnabled(true);
        this.mWebAddressEditText.setEnabled(false);
        this.mWebsNumberEditText.setEnabled(false);
    }

    /**
     * Function to end the process
     */
    private void stop() {
        //Change running flag
        this.mRuiningFlag = false;

        //Clear the old webs
        this.mWebsLinearLayout.removeAllViews();

        //Change text to start and enable
        this.mStartStopButton.setText(R.string.start);

        this.mISeeErrorButton.setEnabled(false);
        this.mWebAddressEditText.setEnabled(true);
        this.mWebsNumberEditText.setEnabled(true);
    }

    /**
     * Function to get params from the user
     * @return success or failed
     */
    private boolean getParams() {
        //Get params
        this.mWebAddress = this.mWebAddressEditText.getText().toString();

        try {
            this.mWebsNumber = Integer.parseInt(this.mWebsNumberEditText.getText().toString());
        } catch (NumberFormatException e) {
            //If the number isn't valid
            Toast.makeText(this, "Invalidate webs number", Toast.LENGTH_SHORT).show();

            return false;
        }

        //Check address validate
        if(!URLUtil.isValidUrl(this.mWebAddress)) {
            Toast.makeText(this, "Invalidate web address", Toast.LENGTH_SHORT).show();

            return false;
        }

        //Check Webs Number
        if(this.mWebsNumber <= 0 || this.mWebsNumber >= 11) {
            Toast.makeText(this, "Invalidate webs number (1 - 10)", Toast.LENGTH_SHORT).show();

            return false;
        }

        return true;
    }

    private void iSeeError(boolean ShowDialog) {
        if(this.mCurrentProxy != null) {
            Proxy.remove(this.mCurrentProxy);

            if(ShowDialog) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Proxy Not Working");
                builder.setMessage("Ip: " + this.mCurrentProxy.mHost + "\n" + "Port: " + this.mCurrentProxy.mPort);
                builder.show();
            }
        }

        build();
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.StartStopButton:
                //Change to mode
                changeRunning();
                break;

            case R.id.ISeeErrorButton:
                iSeeError(true);

                break;
        }

    }

    @Override
    public void run() {
        while(this.mRuiningFlag) {
            //Build web view
            build();

            //Wait
            while(!this.mThreadFinishLoad);
            try { Thread.sleep(TIME_TO_REFRESH); } catch (Exception e) {}
        }
    }

    static class Proxy {
        private static List<Proxy> mProxysList = new ArrayList<>();
        private static Random mRandom = new Random();

        /* Params */
        private String mHost = "";
        private int    mPort = 0;

        public Proxy(String Host, int Port) {
            //Set local params
            this.mHost = Host;
            this.mPort = Port;

        }

        /* Getters Setters */
        public String getHost() {
            return mHost;
        }

        public void setHost(String Host) {
            this.mHost = Host;
        }

        public int getPort() {
            return mPort;
        }

        public void setPort(int Port) {
            this.mPort = Port;
        }

        public static void start() {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    while (true) {
                        try {
                            Thread.sleep(TIME_TO_REFRESH * 10);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        install();
                    }
                }
            }).start();

            install();
        }

        private static void install() {
            try {
                mProxysList = getProxysList();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public static Proxy getRandom() {
            return mProxysList.get(mRandom.nextInt(mProxysList.size()));
        }

        public static void remove(Proxy Proxy) {
            mProxysList.remove(Proxy);

            if(mProxysList.size() <= 20) {
                install();
            }
        }

        /**
         * Function to get proxys list from proxys list web
         * @return List of proxys
         * @throws IOException If failed to connect to the web and get the html data
         *
         */
        public static List<Proxy> getProxysList() throws IOException {
            return getProxysList(null);
        }

        /**
         * Function to get proxys list from proxys list web
         * @param CheckProxySettings Method to check the proxy server params
         * @return List of proxys
         * @throws IOException If failed to connect to the web and get the html data
         *
         */
        public static List<Proxy> getProxysList(CheckProxySettings CheckProxySettings) throws IOException {
            //Check validate
            if(CheckProxySettings == null) {
                //Create new one if null
                CheckProxySettings = DefaultCheckProxySettings;

            }

            List<Proxy> proxysList = new ArrayList<>();

            //Get the table data from the web
            Elements tableElements = Jsoup.connect(WEB_URL)
                    .get()
                    .getElementById(TABLE_ID)
                    .select("tr");

            //Convert the elements list to proxys list
            for (int i = 1; i < tableElements.size(); i++) {
                //Get this index
                Elements thisProxyRow = tableElements.get(i).getAllElements();

                //Check validate
                if(thisProxyRow.get(PROXY_HOST_INDEX).childNodes().size() <= 0) {
                    continue;
                }

                //Check if proxy settings ok
                if(CheckProxySettings.isProxySettingsOK(thisProxyRow)) {
                    proxysList.add(
                            //Create proxy from the row params
                            new Proxy(
                                    thisProxyRow.get(PROXY_HOST_INDEX).childNode(0).toString(),
                                    Integer.parseInt(thisProxyRow.get(PROXY_PORT_INDEX).childNode(0).toString())
                            )
                    );
                }
            }

            return proxysList;
        }

        public static final CheckProxySettings DefaultCheckProxySettings = new CheckProxySettings() {
            @Override
            public boolean isProxySettingsOK(Elements ProxyRow) {
                //Get element to check
                Element thisElement = ProxyRow.get(PROXY_TYPE_INDEX);

                //Check element validate
                if(thisElement.childNodes().size() == 0) {
                    return false;
                }

                //Return the ok or not
                return thisElement.childNode(0).toString().equals("anonymous");

            }
        };

        public interface CheckProxySettings {
            boolean isProxySettingsOK(Elements ProxyRow);

        }
    }
}
